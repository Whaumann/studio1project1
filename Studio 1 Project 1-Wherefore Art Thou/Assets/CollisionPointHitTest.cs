﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionPointHitTest : MonoBehaviour
{
    public GameObject ballTransform;
    public GameObject flowerPrefab;

    ContactPoint contact;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Ball")
        {
            //Will instantiate flower prefab on balls latest transfrom
            Instantiate(flowerPrefab, ballTransform.transform.position, ballTransform.transform.rotation);
            gameObject.transform.localScale += new Vector3(0.1f, 0.1f, 0.1f);
        }
    }
}
