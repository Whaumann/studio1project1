﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{

    public string nextScene;

    private void Update()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void LoadScene(string nextScene)
    {
        SceneManager.LoadScene(nextScene);
    }
}
