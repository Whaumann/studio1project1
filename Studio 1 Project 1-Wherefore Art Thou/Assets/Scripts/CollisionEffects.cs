﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;


public class CollisionEffects : MonoBehaviour
{
    public bool useVFX, useSFX;
    public string collidbleGameObjectName;
    public ParticleSystem CollideVFX;
    public AudioSource CollideSFX;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == collidbleGameObjectName)
        {
            if (useVFX)
            {
                CollideVFX.Play();
            }
            if (useSFX)
            {
                CollideSFX.Play();
            }
        }
    }
}
