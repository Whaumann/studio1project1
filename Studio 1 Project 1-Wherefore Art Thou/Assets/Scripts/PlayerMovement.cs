﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMovement : MonoBehaviour
{



    private Rigidbody2D rb2d;
    public float speed;
    public Camera mainCamera;

    private Vector2 m_vectorLeft = Vector2.left;
    private Vector2 m_vectorRight = Vector2.right;
    private Vector2 m_vectorZero = Vector2.zero;


    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        if (mainCamera == null)
        {
            mainCamera = Camera.main;
        }
    }

    private void FixedUpdate()
    {

        Controller();
    }

    void Controller()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {

            rb2d.velocity = m_vectorLeft * speed * Time.deltaTime;

        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            rb2d.velocity = m_vectorRight * speed * Time.deltaTime;
        }
        else
        {
            rb2d.velocity = m_vectorZero;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            speed += 50;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            speed -= 50;
        }
    }
}
