﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DeathBox : MonoBehaviour
{
    private bool ballIsDestroyed = false;
    public UnityEvent respawnBallEvent;

    IEnumerator ballRespawn;

    // Start is called before the first frame update
    void Start()
    {
        ballRespawn = waitAndRespawn(3f);
    }

    // Update is called once per frame
    void Update()
    {

        /* if (ballIsDestroyed)
         {
             StartCoroutine(ballRespawn);
         } */

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        Destroy(collision.gameObject);
        Debug.Log(collision.gameObject);
        ballIsDestroyed = true;
    }
    public IEnumerator waitAndRespawn(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        respawnBallEvent.Invoke();
        ballIsDestroyed = false;
        StopCoroutine(ballRespawn);

    }

}
