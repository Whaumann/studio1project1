﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTracker : MonoBehaviour
{

    public Transform ballRespawnPoint;
    public GameObject ballPreFab;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ballRespawn()
    {
        Instantiate(ballPreFab, ballRespawnPoint.position, ballRespawnPoint.rotation);
    }
}
